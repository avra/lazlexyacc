# Lazarus Lex and Yacc #

[Original Lazarus Lex and Yacc site](http://www.tcoq.org/composants) has gone into oblivion. Although it is down, you can still find most of it at [Web Archive](https://web.archive.org/web/20130912232708/http://www.tcoq.org:80/composants). Unfortunatelly, online archived manuals are missing images and code download links are dead. This is an effort to preserve Lazarus Lex and Yacc from vanishing completelly. Full code and manuals can be found in download link.

## Download ##
[Lazarus Lex and Yac v1.0](https://bitbucket.org/avra/lazlexyacc/downloads/LazLexYacc_V1.0.zip)